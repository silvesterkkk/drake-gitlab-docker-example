CI_REGISTRY_IMAGE ?= registry.gitlab.com/ecohealthalliance/drake-gitlab-docker-example

all: docker

docker:
	docker build --cache-from $(CI_REGISTRY_IMAGE):latest -t $(CI_REGISTRY_IMAGE):latest . && docker run -v `pwd`:/repo -w /repo $(CI_REGISTRY_IMAGE):latest ./make.R
